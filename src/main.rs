#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]
#![feature(nll)]
extern crate pest;
extern crate llvm;
#[macro_use]
extern crate pest_derive;
#[macro_use]
extern crate clap;

use pest::Parser;

mod parser;
mod codegen;
mod util;

use parser::*;
use parser::ast::*;

use codegen::CodeGeneratorCx;
use codegen::mc::CodeGenerator;

use llvm::context::Context;

use std::fs::File;
use std::io::Read;

fn main() {
    let matches = clap_app!(oxyc =>
        (version: env!("CARGO_PKG_VERSION"))
        (author: env!("CARGO_PKG_AUTHORS"))
        (about: "Compiler for the Oxygen language")
        (@arg output: -o --output +takes_value "Name of the output binary (Defaults to a.out)")
        (@arg optim_level: -O --optimization +takes_value { |n| if vec!["0", "1", "2", "3"].contains(&n.as_str()) { Ok(()) } else { Err(String::from("invalid argument")) } } )
        (@arg ir_output: -S)
        (@arg ast_out: -A)
        (@arg input: +required "The source file to compile")
    ).get_matches();

    let output_file = matches.value_of("output").unwrap_or("a.out");
    let input_file = matches.value_of("input").unwrap();
    let source = {
        let mut file = File::open(input_file).unwrap();
        let mut buf = String::new();
        file.read_to_string(&mut buf).unwrap();

        buf
    };


    let pairs = OxyParser::parse(Rule::module, &source)
        .unwrap_or_else(|e| panic!("{}", e));

    let module = ModuleBuilder::new(pairs).build();

    if matches.is_present("ast_out") {
        println!("{:#?}", module);
        return;
    }

    let optimization_level: u32 = matches.value_of("optim_level").unwrap_or("0").parse().unwrap();

    let cx = Context::new(&input_file);
    let mut codegen= CodeGeneratorCx::new(module, &cx, optimization_level);
    codegen.compile();

    if matches.is_present("ir_output") {
        use std::fs::File;
        use std::io::Write;
        let output_file = match output_file {
            "a.out"  => input_file.replace("ol", "ll"),
            name => format!("{}.ll", name),
        };
        let mut file = File::create(output_file).unwrap();
        file.write_all(cx.get_ir().as_bytes()).unwrap();
        file.flush().unwrap();
    }else {
        let mcgen = CodeGenerator::new(&codegen, &output_file);
        mcgen.generate();
    }
}

