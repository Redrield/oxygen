use llvm::llvm_sys::core::*;
use llvm::type_::*;

use llvm::builder::Builder;

use codegen::typeck::Type;

use util::*;

use parser::*;

use codegen::CodeGeneratorCx;

pub fn build_ret(stmt: Statement, bx: &Builder, cx: &mut CodeGeneratorCx) {
    if let Statement::Return(expr) = stmt {
        let ty = cx.type_for(expr.clone());
        let expr = cx.parse_expression(expr, &bx);
        match ty {
            Type::String => bx.ret(bx.inbounds_gep(expr, &[0.to_llvm(&cx.cx), 0.to_llvm(&cx.cx)])),
            _ => bx.ret(expr),
        }
    }
}

pub fn build_while(stmt: Statement, bx: &Builder, cx: &mut CodeGeneratorCx) {
    if let Statement::While { cond, stmts } = stmt {
        match cond {
            Condition::Binary(lhs, op, rhs) => {
                let true_bx = bx.build_sibling_block();
                cx.build_statements(stmts, &true_bx);

                let block = bx.create_block();
                bx.cond_br(bx.icmp(op.clone().into(), cx.parse_expression(lhs.clone(), &bx),
                                   cx.parse_expression(rhs.clone(), &bx)), true_bx.llbb(), block);
                true_bx.cond_br(true_bx.icmp(op.clone().into(), cx.parse_expression(lhs, &true_bx),
                                             cx.parse_expression(rhs, &true_bx)), true_bx.llbb(), block);

                bx.position_at_end(block);
            }
            Condition::Unary(expr) => match expr {
                Expression::Identifier(ident) => {
                    let ptr = cx.vtable[&ident];
                    let true_bx = bx.build_sibling_block();
                    cx.build_statements(stmts, &true_bx);

                    let block = bx.create_block();
                    bx.cond_br(bx.load(ptr), true_bx.llbb(), block);
                    true_bx.cond_br(true_bx.load(ptr), true_bx.llbb(), block);
                    bx.position_at_end(block);
                }
                Expression::Call { ident, args } => {
                    let true_bx = bx.build_sibling_block();

                    let block = bx.create_block();

                    // Crazy hack about multiple (differing mutability) borrows in the same scope
                    {
                        let llfn = &cx.ftable[&ident];

                        if Type::from(llfn.function_type().ret(), &cx).unwrap() != Type::Bool {
                            panic!("Invalid predicate. Non boolean returning function")
                        }

                        let args = cx.get_args(&args[..], &bx);
                        bx.cond_br(bx.call(**llfn, &args[..], false), true_bx.llbb(), block);
                    }
                    cx.build_statements(stmts, &true_bx);

                    {
                        let llfn = &cx.ftable[&ident];

                        let args = cx.get_args(&args[..], &true_bx);
                        true_bx.cond_br(true_bx.call(**llfn, &args[..], false), true_bx.llbb(), block);
                    }

                    bx.position_at_end(block);
                }
                _ => unreachable!()
            }
        }
    }
}

pub fn build_if(stmt: Statement, bx: &Builder, cx: &mut CodeGeneratorCx) {
    if let Statement::If { cond, if_true, else_if } = stmt {
        match cond {
            Condition::Binary(lhs, op, rhs) => {
                // Parse the operands and fill the first block of our conditional
                let lhs = cx.parse_expression(lhs, &bx);
                let rhs = cx.parse_expression(rhs, &bx);
                let true_bx = bx.build_sibling_block();
                cx.build_statements(if_true.clone(), &true_bx);

                // If there is an associated else statement, fill that and store the builder in else_block
                let mut else_block = None;
                if let Some(else_if) = else_if.clone() {
                    let else_bx = bx.build_sibling_block();
                    cx.build_statements(else_if, &else_bx);
                    else_block = Some(else_bx);
                }

                let block = bx.create_block();

                else_block.operate(|bx| {
                    match else_if.clone().unwrap().last().unwrap() {
                        Statement::Return(_)  => {}
                        _ => {
                            bx.br(block);
                        }
                    }
                });


                // conditional break here. Either break to block or else_block depending on if there is an associated else statement

                match (if_true.last().unwrap(), else_if.map(|vec| vec.last().unwrap().clone())) {
                    (Statement::Return(_), Some(Statement::Return(_))) => {
                        unsafe {
                            LLVMDeleteBasicBlock(block);
                            bx.cond_br(bx.icmp(op.into(), lhs, rhs), true_bx.llbb(), else_block.unwrap().llbb());
                        }
                    }
                    _ => {
                        bx.cond_br(bx.icmp(op.into(), lhs, rhs), true_bx.llbb(), else_block.map(|bx| bx.llbb()).unwrap_or(block));
                        true_bx.br(block);

                        // Execution continues in the exit block
                        bx.position_at_end(block);
                    }
                }

            }
            Condition::Unary(e) => match e {
                Expression::Identifier(i) => {
                    let bool_value = bx.load(cx.vtable[&i]);
                    let true_bx = bx.build_sibling_block();
                    cx.build_statements(if_true, &true_bx);

                    let mut else_block = None;
                    if let Some(else_if) = else_if {
                        let else_bx = bx.build_sibling_block();
                        cx.build_statements(else_if, &else_bx);
                        else_block = Some(else_bx);
                    }

                    let block = bx.create_block();

                    else_block.operate(|bx| {
                        bx.br(block);
                    });

                    bx.cond_br(bool_value, true_bx.llbb(), else_block.map(|bx| bx.llbb()).unwrap_or(block));
                    true_bx.br(block);
                    bx.position_at_end(block);
                }
                Expression::Call { ident, args } => {
                    let true_bx = bx.build_sibling_block();

                    let mut else_block = None;
                    if let Some(else_if) = else_if {
                        let else_bx = bx.build_sibling_block();
                        cx.build_statements(else_if, &else_bx);
                        else_block = Some(else_bx);
                    }

                    let block = bx.create_block();

                    else_block.operate(|bx| {
                        bx.br(block);
                    });

                    // Crazy hack about multiple (differing mutability) borrows in the same scope
                    {
                        let llfn = &cx.ftable[&ident];

                        if Type::from(llfn.function_type().ret(), &cx).unwrap() != Type::Bool {
                            panic!("Invalid predicate. Non boolean returning function")
                        }

                        let args = cx.get_args(&args[..], &bx);

                        bx.cond_br(bx.call(**llfn, &args[..], false), true_bx.llbb(), else_block.map(|bx| bx.llbb()).unwrap_or(block));
                    }
                    cx.build_statements(if_true, &true_bx);
                    true_bx.br(block);
                    bx.position_at_end(block);
                }
                _ => unreachable!()
            }
        }
    }
}