use llvm::llvm_sys::core::*;

use llvm::builder::Builder;
use llvm::type_::*;
use llvm::struct_::*;

use parser::*;

use codegen::CodeGeneratorCx;

pub fn build_struct_definition(stmt: Statement, cx: &mut CodeGeneratorCx) {
    if let Statement::StructDefinition { ident, members } = stmt {
        let members = members.into_iter().map(|(name, ty)| (name, cx.map_type(&ty)))
            .collect::<Vec<(String, Type)>>();
        let ty = StructType::new(ident.clone(), members, &cx.cx);
        cx.struct_table.insert(ident, ty);
    }
}

pub fn build_declaration(stmt: Statement, bx: &Builder, cx: &mut CodeGeneratorCx) {
    if let Statement::Declaration { ident, value } = stmt {
        if let Some(..) = cx.vtable.get(&ident) {
            panic!("Shadowed variable name '{}'", ident);
        }

        let value = cx.parse_expression(value.clone(), &bx);
        let ptr = unsafe {
            let ty = Type::from_ref(LLVMTypeOf(value));
            bx.alloca(ty)
        };
        bx.store(value, ptr);

        cx.vtable.insert(ident.to_string(), ptr);
    }
}

pub fn build_redeclaration(stmt: Statement, bx: &Builder, cx: &mut CodeGeneratorCx) {
    if let Statement::Redeclaration { ident, redecl_op: op, value } = stmt {

        // Code very similar to the code for Declaration, except there's a bit more to keep in mind
        let ptr = match cx.vtable.get(&ident) {
            Some(p) => *p,
            None => panic!("No variable named {}", ident)
        };

        let value = cx.parse_expression(value.clone(), &bx);

        match op {
            // If it's just a normal redeclaration (no strings attached), just store it and move on
            AssignOperation::Normal => {
                bx.store(value, ptr);
            }
            // If it's not, type checking is needed to make sure the variable is an integer
            _ => {
                let current_value = bx.load(ptr);

                let ty = unsafe {
                    Type::from_ref(LLVMTypeOf(current_value))
                };

                if ty != Type::i64(&cx.cx) {
                    panic!("Invalid type for operation");
                }


                // Generates the new value to store in the reference based on the type of assignment operator, then stores it
                let new = match op {
                    AssignOperation::AddAssign => {
                        bx.add(current_value, value)
                    }
                    AssignOperation::SubAssign => {
                        bx.sub(current_value, value)
                    }
                    AssignOperation::MulAssign => {
                        bx.mul(current_value, value)
                    }
                    AssignOperation::DivAssign => {
                        bx.sdiv(current_value, value)
                    }
                    _ => unreachable!()
                };

                bx.store(new, ptr);
            }
        }
    }
}
