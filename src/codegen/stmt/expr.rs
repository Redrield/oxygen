use llvm::builder::Builder;

use codegen::CodeGeneratorCx;

use parser::*;

pub fn build_function_call(expr: Expression, bx: &Builder, cx: &mut CodeGeneratorCx) {
    // The only real top level expression that is wrapped like this is a function call
    if let Expression::Call { ident, args } = expr {
        // Turn our AST Expressions into something we can work with
        let args = cx.get_args(&args[..], &bx);

        let llfn = &cx.ftable[&ident];

        bx.call(**llfn, &args[..], false);
    }
}
