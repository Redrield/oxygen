use llvm::context::Context;
use llvm::builder::Builder;
use llvm::type_::*;
use llvm::func::Function;
use llvm::transform::*;
use llvm::struct_::*;
use ::llvm;
use llvm::llvm_sys::prelude::*;
use llvm::llvm_sys::core::*;

use parser::ast::*;
use parser::*;

use std::collections::HashMap;

pub mod mc;

mod stmt;
mod typeck;

use self::typeck::Type as PType;

/// Struct to convert an AST into a filled LLVM module
#[derive(Debug)]
pub struct CodeGeneratorCx<'a> {
    ast: Module,
    cx: &'a Context,
    optim_level: u32,
    ftable: HashMap<String, Function>,
    vtable: HashMap<String, LLVMValueRef>,
    struct_table: HashMap<String, StructType>,
}

impl<'a> CodeGeneratorCx<'a> {
    pub fn new(ast: Module, cx: &'a Context, optim_level: u32) -> CodeGeneratorCx<'a> {
        let mut _self = CodeGeneratorCx {
            ast,
            cx,
            optim_level,
            ftable: HashMap::new(),
            vtable: HashMap::new(),
            struct_table: HashMap::new(),
        };

        _self.fill_builtins();

        _self
    }

    /// Fills the function and variable tables with builtin values.
    fn fill_builtins(&mut self) {
        let printf = FunctionType::new(Type::i32(&self.cx), vec![Type::i8(&self.cx).ptr_to()], true);
        let printf = llvm::declare_fn(self.cx, "printf", printf);

        self.ftable.insert("printf".to_string(), printf);
    }

    fn build_structs(&mut self) {
        for def in self.ast.clone().iter() {
            if let Statement::StructDefinition { .. } = *def {
                stmt::decl::build_struct_definition(def.clone(), self);
            }
        }
    }

    /// Function to construct and fill all of the function bodies in our module
    /// Filters out function declarations from the top level of the AST, and creates another code generator to compile them
    /// The vtable of this code generator is used to store references to the parameters of the function, as well as any variables declared inside.
    fn build_functions(&mut self, cx: &Context) {
        for func in self.ast.clone().iter() {
            if let Statement::FunctionDeclaration { ref ident, ref ret, ref params, ref body } = *func {
                let fn_ret = self.map_type(ret);
                let params: Vec<(String, Type)> = params.iter().map(|&(ref name, ref ty)| (name.to_string(), self.map_type(ty))).collect();
                let fn_ty = FunctionType::new(fn_ret, params.iter().map(|&(_, ty)| ty).collect(), false);
                let llfn = llvm::declare_fn(cx, &ident, fn_ty);
                let bx = Builder::new_block(cx, *llfn);

                let mut new_vtable = HashMap::new();
                for (i, &(ref param_name, _)) in params.iter().enumerate() {
                    unsafe {
                        let param = LLVMGetParam(*llfn, i as u32);

                        let ptr = {
                            let ty = Type::from_ref(LLVMTypeOf(param));
                            bx.alloca(ty)
                        };

                        bx.store(param, ptr);

                        new_vtable.insert(param_name.clone(), ptr);
                    }
                }

                self.ftable.insert(ident.to_string(), llfn);

                let mut fncx = CodeGeneratorCx {
                    ast: Module::new(body.into_iter().map(|b| *b.clone()).collect()),
                    cx: self.cx,
                    optim_level: 0,
                    ftable: self.ftable.clone(),
                    vtable: new_vtable,
                    struct_table: self.struct_table.clone(),
                };

                fncx.build_statements(fncx.ast.stmts.clone(), &bx);

                if let ParamType::Void = ret {
                    bx.ret_void();
                }
            }
        }
    }

    // Gives a type mapping from our enum to LLVM types.
    fn map_type(&self, ty: &ParamType) -> Type {
        match *ty {
            ParamType::String => Type::i8(&self.cx).ptr_to(),
            ParamType::Int => Type::i64(&self.cx),
            ParamType::Bool => Type::i1(&self.cx),
            ParamType::Void => Type::void(&self.cx),
            ParamType::Custom(ref name) => Type::from_ref(self.struct_table[name].rf)
        }
    }

    /// Compiles the given [Module] into an LLVM Context and Module with entry point main(i32, i8**)
    pub fn compile(&mut self) {
        let main = FunctionType::new(Type::i32(&self.cx), vec![Type::i32(&self.cx), Type::i8(&self.cx).ptr_to().ptr_to()], false);
        let main = llvm::declare_fn(self.cx, "main", main);

        self.build_structs();
        self.build_functions(self.cx);

        let bx = Builder::new_block(self.cx, *main);
        self.build_statements(self.ast.stmts.clone(), &bx);

        bx.ret(0i32.to_llvm(self.cx));

        let mut pmb = PassManagerBuilder::new();
        pmb.set_opt_level(self.optim_level);
        let pm = pmb.build();
        pm.add_pass(PassType::GlobalDCE);
        pm.add_pass(PassType::StripSymbols);

        self.cx.verify();
        pm.run(self.cx);
        self.cx.verify();
    }

    /// Iterates over all Statements in [stmts] and compiles them in turn with the provided Builder
    fn build_statements(&mut self, stmts: Vec<Statement>, bx: &Builder) {
        for stmt in stmts {
            match stmt {
                decl @ Statement::Declaration { .. } => stmt::decl::build_declaration(decl, bx, self),
                decl @ Statement::Redeclaration { .. } => stmt::decl::build_redeclaration(decl, bx, self),
                Statement::Expression(expr) => stmt::expr::build_function_call(expr, bx, self),
                stmt @ Statement::While { .. } => stmt::control::build_while(stmt, bx, self),
                stmt @ Statement::If { .. } => stmt::control::build_if(stmt, bx, self),
                stmt @ Statement::Return(_) => stmt::control::build_ret(stmt, bx, self),
                _ => {}
            }
        }
    }

    fn type_for(&self, expr: Expression) -> PType {
        match expr {
            Expression::StringLiteral(_) => PType::String,
            Expression::IntegerLiteral(_) | Expression::ArithmeticExpression(_, _, _) => PType::Int,
            Expression::BooleanLiteral(_) => PType::Bool,
            Expression::Call { ident, .. } => {
                let llfn = &self.ftable[&ident];

                PType::from(llfn.function_type().ret(), &self).unwrap()
            }
            Expression::Predicate(_) => PType::Bool,
            Expression::Identifier(ident) => {
                let var = self.vtable[&ident];

                let raw = unsafe {
                    LLVMGetAllocatedType(var)
                };

                PType::from(Type::from_ref(raw), &self).unwrap()
            }
            Expression::StructMemberAccess { var_ident, member_ident } => {
                let var = self.vtable[&var_ident];

                unsafe {
                    let sty = self.struct_table.values().find(|sty| sty.rf == LLVMGetAllocatedType(var)).unwrap();
                    let idx = sty.members.iter().position(|ident| *ident == member_ident).unwrap();

                    PType::from(sty.member_types[idx], &self).unwrap()
                }
            }
            Expression::StructInstantiation { ident, .. } => {
                PType::from(Type::from_ref(self.struct_table[&ident].rf), &self).unwrap()
            }
            _ => panic!("Unknown type for expression {:?}", expr)
        }
    }

    /// Turn an AST Expression into an [LLVMValueRef] in the given [Builder]
    fn parse_expression(&self, expr: Expression, bx: &Builder) -> LLVMValueRef {
        match expr {
            Expression::StructMemberAccess { var_ident, member_ident } => {
                let var = self.vtable[&var_ident];

                let stype = self.struct_table.values().find(|stype| unsafe { stype.rf == LLVMGetAllocatedType(var) })
                    .unwrap();

                let index = stype.members.iter().position(|x| *x == member_ident).unwrap();
                let value = bx.struct_gep(var, index as u64);
                bx.load(value)
            }
            Expression::StructInstantiation { ident, values: members } => {
                let s = &self.struct_table[&ident];

                s.instantiate(members.iter().map(|e| {
                    self.parse_expression(*e.clone(), &bx)
                }).collect(), bx, &self.cx).rf
            }
            Expression::StringLiteral(ref s) => {
                bx.global_string(s)
            }
            Expression::BooleanLiteral(b) => {
                Type::i1(&bx.llcx).const_int(b as i32)
            }
            Expression::IntegerLiteral(ref i) => {
                i.to_llvm(&self.cx)
            }
            Expression::Call { ref ident, ref args } => {
                let args = self.get_args(&args[..], &bx);

                let llfn = &self.ftable[ident];
                bx.call(**llfn, &args[..], false)
            }
            Expression::Identifier(ref ident) => {
                let value = self.vtable.get(ident).unwrap_or_else(|| panic!("Variable undefined '{}'", ident));
                bx.load(*value)
            }
            Expression::Predicate(comp) => match *comp {
                Condition::Binary(lhs, op, rhs) => {
                    let lhs = self.parse_expression(lhs, &bx);
                    let rhs = self.parse_expression(rhs, &bx);

                    bx.icmp(op.into(), lhs, rhs)
                }
                _ => unimplemented!()
            }
            Expression::ArithmeticExpression(lhs, op, rhs) => {
                let lhs = self.parse_expression(*lhs, &bx);
                let rhs = self.parse_expression(*rhs, &bx);

                match op {
                    Operation::Add => bx.add(lhs, rhs),
                    Operation::Sub => bx.sub(lhs, rhs),
                    Operation::Mul => bx.mul(lhs, rhs),
                    Operation::Div => bx.sdiv(lhs, rhs)
                }
            }
        }
    }

    fn get_args(&self, args: &[Expression], bx: &Builder) -> Vec<LLVMValueRef> {
        let zero = 0i32.to_llvm(&self.cx);
        let types: Vec<PType> = args.iter().map(|e| self.type_for(e.clone())).collect();
        args.iter().map(|e| self.parse_expression(e.clone(), &bx))
            .zip(types)
            .map(|(val, ty)| {
                match ty {
                    PType::String => unsafe {
                        if LLVMIsGlobalConstant(val) == 1 {
                            bx.inbounds_gep(val, &[zero, zero])
                        } else {
                            val
                        }
                    },
                    _ => match self.vtable.values().find(|&v| *v == val) {
                        Some(..) => bx.load(val),
                        None => val
                    }
                }
            })
            .collect()
    }
}
