use llvm::codegen::CodeGenerator as CodeGen;
use super::*;

pub struct CodeGenerator<'a> {
    inner: CodeGen<'a>,
    output_name: String
}

impl<'a> CodeGenerator<'a> {
    pub fn new(cx: &CodeGeneratorCx<'a>, output_name: &str) -> CodeGenerator<'a> {
        let inner = CodeGen::new(cx.cx);

        CodeGenerator {
            inner,
            output_name: output_name.to_string()
        }
    }

    pub fn generate(&self) {
        self.inner.generate(&self.output_name);
    }
}
