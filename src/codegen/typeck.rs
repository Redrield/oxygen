use llvm::type_::Type as LType;

use codegen::CodeGeneratorCx;

#[derive(Debug, PartialEq)]
pub enum Type {
    String,
    Bool,
    Int,
    Struct(String),
}

impl Type {
    pub fn from(ty: LType, cx: &CodeGeneratorCx) -> Option<Type> {
        if ty == LType::i8(&cx.cx).ptr_to() {
            Some(Type::String)
        } else if ty == LType::i64(&cx.cx) {
            Some(Type::Int)
        } else if ty == LType::i1(&cx.cx) {
            Some(Type::Bool)
        } else if let Some(struct_type) = cx.struct_table.values().find(|s| LType::from_ref(s.rf) == ty) {
            Some(Type::Struct(struct_type.name.clone()))
        }else {
            None
        }
    }
}
