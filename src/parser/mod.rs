
#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct OxyParser;

pub mod ast;

use llvm::llvm_sys::LLVMIntPredicate;

#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
    StructDefinition {
        ident: String,
        members: Vec<(String, ParamType)>,
    },
    Declaration {
        ident: String,
        value: Expression,
    },
    Redeclaration {
        ident: String,
        redecl_op: AssignOperation,
        value: Expression,
    },
    Expression(Expression),
    If {
        cond: Condition,
        if_true: Vec<Statement>,
        else_if: Option<Vec<Statement>>,
    },
    While {
        cond: Condition,
        stmts: Vec<Statement>
    },
    FunctionDeclaration {
        ident: String,
        ret: ParamType,
        params: Vec<(String, ParamType)>,
        body: Vec<Box<Statement>>,
    },
    Return(Expression),
}

#[derive(Debug, Clone, PartialEq)]
pub enum ParamType {
    String,
    Int,
    Bool,
    Void,
    Custom(String),
}

impl From<String> for ParamType {
    fn from(s: String) -> ParamType {
        match s.as_str() {
            "string" => ParamType::String,
            "int" => ParamType::Int,
            "bool" => ParamType::Bool,
            _ => ParamType::Custom(s),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
#[allow(unused)]
pub enum Expression {
    StringLiteral(String),
    IntegerLiteral(i64),
    BooleanLiteral(bool),
    Call {
        ident: String,
        args: Vec<Expression>,
    },
    StructInstantiation {
        ident: String,
        values: Vec<Box<Expression>>,
    },
    StructMemberAccess {
        var_ident: String,
        member_ident: String,
    },
    Identifier(String),
    Predicate(Box<Condition>),
    ArithmeticExpression(Box<Expression>, Operation, Box<Expression>)
}


#[derive(Debug, Clone, PartialEq)]
#[allow(unused)]
pub enum Condition {
    Binary(Expression, CmpOperation, Expression),
    Unary(Expression),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Operation {
    Add,
    Sub,
    Mul,
    Div
}

#[derive(Debug, Clone, PartialEq)]
pub enum CmpOperation {
    Eq,
    Ne,
    Ge,
    Le,
    Gt,
    Lt,
}

impl CmpOperation {
    pub fn from_rule(rule: Rule) -> CmpOperation {
        match rule {
            Rule::cmp_eq => CmpOperation::Eq,
            Rule::cmp_ne => CmpOperation::Ne,
            Rule::cmp_lt => CmpOperation::Lt,
            Rule::cmp_gt => CmpOperation::Gt,
            Rule::cmp_ge => CmpOperation::Ge,
            Rule::cmp_le => CmpOperation::Le,
            _ => unreachable!()
        }
    }
}

impl Into<LLVMIntPredicate> for CmpOperation {
    fn into(self) -> LLVMIntPredicate {
        match self {
            CmpOperation::Eq => LLVMIntPredicate::LLVMIntEQ,
            CmpOperation::Ne => LLVMIntPredicate::LLVMIntNE,
            CmpOperation::Ge => LLVMIntPredicate::LLVMIntSGE,
            CmpOperation::Le => LLVMIntPredicate::LLVMIntSLE,
            CmpOperation::Gt => LLVMIntPredicate::LLVMIntSGT,
            CmpOperation::Lt => LLVMIntPredicate::LLVMIntSLT,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum AssignOperation {
    Normal,
    AddAssign,
    SubAssign,
    MulAssign,
    DivAssign,
}
