use pest::iterators::{Pairs, Pair};

use std::slice::Iter;

use super::*;

#[derive(Debug, PartialEq, Clone)]
/// Representation of a parsed source file.
pub struct Module {
    pub stmts: Vec<Statement>
}

impl Module {
    /// Creates a new [Module] from the given [stmts]
    pub fn new(stmts: Vec<Statement>) -> Module {
        Module {
            stmts
        }
    }

    pub fn iter(&self) -> Iter<Statement> {
        self.stmts.iter()
    }
}

/// Intermediary struct for creation of a [Module].
pub struct ModuleBuilder<'a> {
    pairs: Pairs<'a, Rule>,
    stmts: Vec<Statement>,
}

impl<'a> ModuleBuilder<'a> {
    pub fn new(pairs: Pairs<Rule>) -> ModuleBuilder {
        ModuleBuilder {
            pairs,
            stmts: Vec::new(),
        }
    }

    /// Using [self.pairs], create a `Vec<Statement>` and wrap in a [Module]
    pub fn build(&mut self) -> Module {
        for pair in self.pairs.clone() {
            // Kind of a hacky workaround to the fact that all
            // top level pairs are going to be statements.
            if let Some(stmt) = self.parse_statement(pair.into_inner()) {
                self.stmts.push(stmt);
            }
        }

        // After iterating through all the pairs, this will have been filled with the AST. Just wrap it and return.
        Module::new(self.stmts.clone())
    }

    /// Expands the println! macro from source files into a call to fputs with a full string
    fn expand_println(&mut self, pairs: &Pairs<Rule>) -> Expression {
        let args = self.parse_args(pairs.clone());

        Expression::Call {
            ident: "printf".to_string(),
            args,
        }
    }

    fn parse_struct_definition(&mut self, mut pairs: Pairs<Rule>) -> Statement {
        let struct_ident = pairs.next().unwrap().as_str().to_string();

        let mut members = Vec::new();
        let mut current_member_ident = None;

        for pair in pairs {
            match pair.as_rule() {
                Rule::identifier => match current_member_ident {
                    None => current_member_ident = Some(pair.as_str().to_string()),
                    Some(..) => {
                        let ty = ParamType::from(pair.as_str().to_string());
                        members.push((current_member_ident.clone().unwrap(), ty));
                        current_member_ident = None;
                    }
                }
                _ => unreachable!()
            }
        }

        Statement::StructDefinition {
            ident: struct_ident,
            members
        }
    }

    fn parse_function_declaration(&mut self, mut pairs: Pairs<Rule>) -> Statement {
        let function_ident = pairs.next().unwrap().as_str();
        let mut args = Vec::new();
        let mut body = Vec::new();
        let mut ret = None;

        for pair in pairs {
            match pair.as_rule() {
                Rule::identifier => ret = Some(ParamType::from(pair.as_str().to_string())),
                Rule::arg => {
                    let mut pairs = pair.into_inner();
                    let arg_name = pairs.next().unwrap().as_str();
                    let arg_type = ParamType::from(pairs.next().unwrap().as_str().to_string());

                    args.push((arg_name.to_string(), arg_type))
                }
                Rule::statement => {
                    if let Some(stmt) = self.parse_statement(pair.into_inner()) {
                        body.push(Box::new(stmt))
                    }
                }
                _ => {}
            }
        }

        Statement::FunctionDeclaration {
            ident: function_ident.to_string(),
            ret: ret.unwrap_or(ParamType::Void),
            params: args,
            body,
        }
    }

    fn parse_args(&mut self, pairs: Pairs<Rule>) -> Vec<Expression> {
        let mut acc = Vec::new();

        // Sorta gross, but flattening the tree leads to issues.
        for pair in pairs {
            match pair.as_rule() {
                Rule::expr => for pair in pair.clone().into_inner() {
                    acc.push(self.parse_expression(&pair));
                },
                Rule::string_literal => {
                    if let Expression::StringLiteral(str) = self.parse_expression(&pair) {
                        acc.push(Expression::StringLiteral(format!("{}\n", str)));
                    }
                }
                _ => {}
            }
        }

        acc
    }

    /// Parsed a function call from pairs
    fn parse_function_call(&mut self, pairs: Pairs<Rule>) -> Expression {
        // Accumulators for the function properties
        let mut function_ident = "";
        let mut args = Vec::new();

        for pair in pairs {
            match pair.as_rule() {
                // Top level identifier is just the function name
                Rule::identifier => function_ident = pair.as_str(),
                Rule::expr => {
                    // We're in the function body, find the child rule of this expression and match against that to fill the args
                    let pair = pair.clone().into_inner().next().unwrap();
                    args.push(self.parse_expression(&pair));
                }
                // Push the statement and jump out
                _ => {}
            }
        }

        Expression::Call {
            ident: function_ident.to_string(),
            args,
        }
    }

    fn parse_redeclaration(&mut self, pairs: Pairs<Rule>) -> Statement {
        let mut variable_ident = "";
        let mut variable_expr = None;
        let mut assign_operation = None;

        for pair in pairs {
            match pair.as_rule() {
                Rule::identifier => variable_ident = pair.clone().into_span().as_str(),
                Rule::op_assign => assign_operation = Some(AssignOperation::Normal),
                Rule::op_add_assign => assign_operation = Some(AssignOperation::AddAssign),
                Rule::op_sub_assign => assign_operation = Some(AssignOperation::SubAssign),
                Rule::op_mul_assign => assign_operation = Some(AssignOperation::MulAssign),
                Rule::op_div_assign => assign_operation = Some(AssignOperation::DivAssign),
                Rule::expr => {
                    let inner = pair.clone().into_inner().nth(0).unwrap();
                    variable_expr = Some(self.parse_expression(&inner));
                }
                Rule::semi => break,
                _ => {}
            }
        }

        Statement::Redeclaration {
            ident: variable_ident.to_string(),
            redecl_op: assign_operation.unwrap(),
            value: variable_expr.unwrap(),
        }
    }

    /// Parses a variable declaration from the given [pairs]
    fn parse_variable(&mut self, pairs: Pairs<Rule>) -> Statement {
        // Declare some accumulators for the different parts of the variable
        let mut variable_ident = "";
        let mut variable_expr: Option<Expression> = None;

        // A declaration is parsed into a tree, containing an ident, an op_assign, and an expression.
        // This lets us unbox it into those components
        for inner_pair in pairs {
            match inner_pair.as_rule() {
                // Store the identifier in the accumulator if that's what it is
                Rule::identifier => variable_ident = inner_pair.clone().into_span().as_str(),
                // Bit more fun for an expression, it can again be broken down into more fundamental pieces, which is what we do
                Rule::expr => {
                    let inner = inner_pair.clone().into_inner().nth(0).unwrap();
                    variable_expr = Some(self.parse_expression(&inner));
                }
                // Denotes the end of the statement, we trust that our accumulators have been filled and pop it into the statements vector
                Rule::semi => {
                    break;
                }
                Rule::op_assign => {}
                _ => unreachable!()
            }
        }

        Statement::Declaration {
            ident: variable_ident.to_string(),
            value: variable_expr.unwrap(),
        }
    }

    fn parse_arithmetic_expr(&mut self, pairs: Pairs<Rule>) -> Expression {
        let mut lhs = None;
        let mut rhs = None;
        let mut op = None;

        for pair in pairs {
            match pair.as_rule() {
                Rule::add => op = Some(Operation::Add),
                Rule::sub => op = Some(Operation::Sub),
                Rule::mul => op = Some(Operation::Mul),
                Rule::div => op = Some(Operation::Div),
//                Rule::struct_member_access => {
//                }
//                Rule::integer_literal => {
//                    if lhs.is_none() {
//                        lhs = Some(Expression::IntegerLiteral(pair.as_str().parse().unwrap()));
//                    } else {
//                        rhs = Some(Expression::IntegerLiteral(pair.as_str().parse().unwrap()));
//                    }
//                }
//                Rule::identifier => {
//                    if lhs.is_none() {
//                        lhs = Some(Expression::Identifier(pair.as_str().to_string()))
//                    } else {
//                        rhs = Some(Expression::Identifier(pair.as_str().to_string()))
//                    }
//                }
//                Rule::func_call => {
//                    if lhs.is_none() {
//                        lhs = Some(self.parse_function_call(pair.clone().into_inner()))
//                    } else {
//                        rhs = Some(self.parse_function_call(pair.clone().into_inner()))
//                    }
//                }
                _ => {
                    if lhs.is_none() {
                        lhs = Some(self.parse_expression(&pair));
                    }else {
                        rhs = Some(self.parse_expression(&pair));
                    }
                }
            }
        }

        Expression::ArithmeticExpression(Box::new(lhs.unwrap()), op.unwrap(), Box::new(rhs.unwrap()))
    }

    // Used to parse a while loop
    fn parse_while(&mut self, mut pairs: Pairs<Rule>) -> Statement {
        let pair = pairs.next().unwrap();
        let predicate = match pair.as_rule() {
            Rule::identifier => Condition::Unary(Expression::Identifier(pair.as_str().to_string())),
            Rule::func_call => Condition::Unary(self.parse_function_call(pair.clone().into_inner())),
            _ => self.parse_predicate(pair.into_inner())
        };

        let mut stmts = Vec::new();

        for pair in pairs {
            if let Some(stmt) = self.parse_statement(pair.into_inner()) {
                stmts.push(stmt);
            }
        }

        Statement::While {
            cond: predicate,
            stmts,
        }
    }

    // Used to parse an if (and optional else) statment pair into a tree of parsed statements
    fn parse_if(&mut self, mut pairs: Pairs<Rule>) -> Statement {
        let pair = pairs.next().unwrap();
        let predicate = match pair.as_rule() {
            Rule::identifier => Condition::Unary(Expression::Identifier(pair.as_str().to_string())),
            Rule::func_call => Condition::Unary(self.parse_function_call(pair.clone().into_inner())),
            _ => self.parse_predicate(pair.into_inner())
        };

        let mut cond_true = Vec::new();
        let mut cond_false = Vec::new();

        for pair in pairs {
            if pair.as_rule() == Rule::else_stmt {
                for pair in pair.clone().into_inner() {
                    if let Some(stmt) = self.parse_statement(pair.into_inner()) {
                        cond_false.push(stmt);
                    }
                }
                continue;
            }
            if let Some(stmt) = self.parse_statement(pair.into_inner()) {
                cond_true.push(stmt);
            }
        }

        Statement::If {
            cond: predicate,
            if_true: cond_true,
            else_if: if cond_false.is_empty() { None } else { Some(cond_false) },
        }
    }

    // Used to parse a boolean expression.
    fn parse_predicate(&mut self, mut pairs: Pairs<Rule>) -> Condition {
        let pair = pairs.next().unwrap();
        let mut pairs = pair.clone().into_inner();
        let lhs_pair = pairs.next().unwrap();
        let lhs = self.parse_expression(&lhs_pair);

        let rhs_pair = pairs.next().unwrap();
        let rhs = self.parse_expression(&rhs_pair);
        let op = CmpOperation::from_rule(pair.as_rule());

        Condition::Binary(lhs, op, rhs)
    }

    fn parse_statement(&mut self, mut pairs: Pairs<Rule>) -> Option<Statement> {
        // Wrapped in an option to get rust to allow this construct. It's a bit messy but it works
        let mut stmt: Option<Statement> = None;
        let pair = pairs.next()?;
        // We're working with the top level of the tree pest gives us, All of the rules here will be statements
        match pair.as_rule() {
            Rule::return_stmt => {
                let expr = pair.into_inner().next().unwrap().into_inner().next().unwrap();

                stmt = Some(Statement::Return(self.parse_expression(&expr)));
            }
            // Most of the functionality for parsing complex constructs is moved into other functions
            Rule::struct_definition => stmt = Some(self.parse_struct_definition(pair.clone().into_inner())),
            Rule::declaration => stmt = Some(self.parse_variable(pair.clone().into_inner())),
            Rule::redeclaration => stmt = Some(self.parse_redeclaration(pair.clone().into_inner())),
            Rule::if_stmt => stmt = Some(self.parse_if(pair.clone().into_inner())),
            Rule::while_stmt => stmt = Some(self.parse_while(pair.clone().into_inner())),
            Rule::function_declaration => stmt = Some(self.parse_function_declaration(pair.clone().into_inner())),
            Rule::expr => {
                let pair = pair.into_inner().next().unwrap();

                stmt = Some(Statement::Expression(self.parse_expression(&pair)));

            }
            _ => {}
        }

        stmt
    }

    fn parse_expression(&mut self, expr: &Pair<Rule>) -> Expression {
        match expr.as_rule() {
            Rule::func_call => self.parse_function_call(expr.clone().into_inner()),
            Rule::struct_instantiation => {
                let mut pairs = expr.clone().into_inner();
                let ident = pairs.next().unwrap().as_str().to_string();
                let mut members = Vec::new();

                for pair in pairs {
                    if let Rule::expr = pair.as_rule() {
                        members.push(Box::new(self.parse_expression(&pair.into_inner().nth(0).unwrap())));
                    }
                }

                Expression::StructInstantiation {
                    ident,
                    values: members
                }
            }
            Rule::struct_member_access => {
                let mut pairs = expr.clone().into_inner();
                let var_ident = pairs.next().unwrap().as_str().to_string();
                let member_ident = pairs.next().unwrap().as_str().to_string();

                Expression::StructMemberAccess {
                    var_ident,
                    member_ident
                }
            }
            Rule::identifier => Expression::Identifier(expr.as_str().to_string()),
            Rule::string_literal => Expression::StringLiteral(expr.as_str().replace("\"", "")),
            Rule::integer_literal => Expression::IntegerLiteral(expr.as_str().parse().unwrap()),
            Rule::boolean_literal => Expression::BooleanLiteral(expr.as_str().parse().unwrap()),
            Rule::predicate => Expression::Predicate(Box::new(self.parse_predicate(expr.clone().into_inner()))),
            Rule::arithmetic_expr => self.parse_arithmetic_expr(expr.clone().into_inner()),
            Rule::println_call => self.expand_println(&expr.clone().into_inner()),
            other => unreachable!("Unrecognized operation {:?}", other),
        }
    }
}

#[cfg(test)]
mod tests {
    use ::parser::*;
    use super::*;

    use pest::Parser;

    #[test]
    fn test_variable_declaration() {
        let pairs = OxyParser::parse(Rule::module, r#"let x = "Hello";"#).unwrap();
        let module = ModuleBuilder::new(pairs).build();

        assert_eq!(module, Module::new(vec![
            Statement::Declaration {
                ident: "x".to_string(),
                value: Expression::StringLiteral("Hello".to_string()),
            }
        ]));
    }

    #[test]
    fn test_function_call() {
        let pairs = OxyParser::parse(Rule::module, "func(arg1, arg2);").unwrap();
        let module = ModuleBuilder::new(pairs).build();

        assert_eq!(module, Module::new(vec![
            Statement::Expression(
                Expression::Call {
                    ident: "func".to_string(),
                    args: vec![
                        Expression::Identifier("arg1".to_string()),
                        Expression::Identifier("arg2".to_string())
                    ],
                }
            )
        ]))
    }

    #[test]
    fn test_multi_statement_ast() {
        let pairs = OxyParser::parse(Rule::module, r#"
            let x = "Hello";
            func(x);
        "#).unwrap();
        let module = ModuleBuilder::new(pairs).build();

        assert_eq!(module, Module::new(vec![
            Statement::Declaration {
                ident: "x".to_string(),
                value: Expression::StringLiteral("Hello".to_string()),
            },
            Statement::Expression(
                Expression::Call {
                    ident: "func".to_string(),
                    args: vec![Expression::Identifier("x".to_string())],
                }
            )
        ]))
    }
}