pub trait Take<T> {
    fn operate<F: FnOnce(&mut T)>(&mut self, f: F);
}

impl<T> Take<T> for Option<T> {
    fn operate<F: FnOnce(&mut T)>(&mut self, f: F) {
        if let Some(ref mut t) = *self {
            f(t);
        }
    }
}
