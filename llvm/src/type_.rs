use std::ffi::{CString, CStr};
use std::fmt;
use std::mem;
use std::ptr;
use std::ops::Deref;

use libc;

use builder;

use llvm_sys::prelude::*;
use llvm_sys::core::*;

use context::Context;

#[derive(Copy, Clone, PartialEq)]
#[repr(C)]
pub struct Type {
    rf: LLVMTypeRef
}

#[derive(Clone, PartialEq, Debug)]
pub struct FunctionType {
    ret: Type,
    args: Vec<Type>,
    vararg: bool,
}

impl fmt::Debug for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ty = unsafe {
            let ptr = LLVMPrintTypeToString(**self);
            let msg = CStr::from_ptr(ptr).to_string_lossy().into_owned();
            LLVMDisposeMessage(ptr);

            msg
        };

        f.write_str(&ty)
    }
}

macro_rules! ty {
    ($e:expr) => (Type::from_ref(unsafe { $e }))
}

impl Deref for Type {
    type Target = LLVMTypeRef;

    fn deref(&self) -> &Self::Target {
        &self.rf
    }
}

impl Type {
    #[inline(always)]
    pub fn from_ref(rf: LLVMTypeRef) -> Type {
        Type {
            rf
        }
    }

    #[inline(always)]
    #[deprecated(note = "Use std::ops::Deref instead")]
    pub fn to_ref(&self) -> LLVMTypeRef {
        self.rf
    }

    pub fn to_ref_slice(slice: &[Type]) -> &[LLVMTypeRef] {
        unsafe {
            mem::transmute(slice)
        }
    }

    pub fn void(cx: &Context) -> Type {
        ty!(LLVMVoidTypeInContext(cx.llcx))
    }

    pub fn i1(cx: &Context) -> Type {
        ty!(LLVMInt1TypeInContext(cx.llcx))
    }

    pub fn i8(cx: &Context) -> Type {
        ty!(LLVMInt8TypeInContext(cx.llcx))
    }
    pub fn i16(cx: &Context) -> Type {
        ty!(LLVMInt16TypeInContext(cx.llcx))
    }

    pub fn i32(cx: &Context) -> Type {
        ty!(LLVMInt32TypeInContext(cx.llcx))
    }

    pub fn i64(cx: &Context) -> Type {
        ty!(LLVMInt64TypeInContext(cx.llcx))
    }

    pub fn i128(cx: &Context) -> Type {
        ty!(LLVMIntTypeInContext(cx.llcx, 128))
    }

    pub fn ix(cx: &Context, num_bits: u64) -> Type {
        ty!(LLVMIntTypeInContext(cx.llcx, num_bits as libc::c_uint))
    }

    pub fn f32(cx: &Context) -> Type {
        ty!(LLVMFloatTypeInContext(cx.llcx))
    }

    pub fn f64(cx: &Context) -> Type {
        ty!(LLVMDoubleTypeInContext(cx.llcx))
    }

    pub fn bool(cx: &Context) -> Type {
        Type::i8(cx)
    }

    pub fn char(cx: &Context) -> Type {
        Type::i32(cx)
    }

    pub fn func(args: &[Type], ret: &Type) -> Type {
        let slice: &[LLVMTypeRef] = Type::to_ref_slice(args);
        ty!(LLVMFunctionType(**ret, slice.as_ptr() as *mut LLVMTypeRef, slice.len() as libc::c_uint, builder::FALSE))
    }

    pub fn variadic_func(args: &[Type], ret: &Type) -> Type {
        let slice: &[LLVMTypeRef] = Type::to_ref_slice(args);
        ty!(LLVMFunctionType(**ret, slice.as_ptr() as *mut LLVMTypeRef, slice.len() as libc::c_uint, builder::TRUE))
    }

    pub fn struct_(cx: &Context, els: &[Type], packed: bool) -> Type {
        let packed = if packed {
            builder::TRUE
        } else {
            builder::FALSE
        };

        let els: &[LLVMTypeRef] = Type::to_ref_slice(els);
        ty!(LLVMStructTypeInContext(cx.llcx, els.as_ptr() as *mut LLVMTypeRef, els.len() as libc::c_uint, packed))
    }

    pub fn named_struct(cx: &Context, name: &str) -> Type {
        let name = CString::new(name).unwrap();
        ty!(LLVMStructCreateNamed(cx.llcx, name.as_ptr()))
    }

    pub fn array(ty: &Type, len: u64) -> Type {
        ty!(LLVMArrayType(**ty, len as libc::c_uint))
    }

    pub fn vector(ty: &Type, len: u64) -> Type {
        ty!(LLVMVectorType(**ty, len as libc::c_uint))
    }

    pub fn set_struct_body(&mut self, els: &[Type], packed: bool) {
        let slice: &[LLVMTypeRef] = Type::to_ref_slice(els);
        let packed = if packed {
            builder::TRUE
        } else {
            builder::FALSE
        };

        unsafe {
            LLVMStructSetBody(**self, slice.as_ptr() as *mut LLVMTypeRef,
                              els.len() as libc::c_uint, packed);
        }
    }

    pub fn ptr_to(&self) -> Type {
        ty!(LLVMPointerType(**self, 0))
    }

    pub fn element_type(&self) -> Type {
        unsafe {
            Type::from_ref(LLVMGetElementType(**self))
        }
    }

    pub fn vector_length(&self) -> usize {
        unsafe {
            LLVMGetVectorSize(**self) as usize
        }
    }

    pub fn func_params(&self) -> Vec<Type> {
        unsafe {
            let nargs = LLVMCountParamTypes(**self) as usize;
            let mut args = vec![Type { rf: ptr::null_mut() }; nargs];
            LLVMGetParamTypes(**self,
                              args.as_mut_ptr() as *mut LLVMTypeRef);
            args
        }
    }

    pub fn int_width(&self) -> u64 {
        unsafe {
            LLVMGetIntTypeWidth(**self) as u64
        }
    }

    pub fn const_int(&self, i: i32) -> LLVMValueRef {
        unsafe {
            LLVMConstInt(**self, i as libc::c_ulonglong, false as i32)
        }
    }
}

impl FunctionType {
    pub fn new(ret: Type, args: Vec<Type>, vararg: bool) -> FunctionType {
        FunctionType {
            ret,
            args,
            vararg,
        }
    }

    pub fn to_ref(&self) -> LLVMTypeRef {
        let ret = *self.ret;
        let args: Vec<LLVMTypeRef> = self.args.iter().map(|arg| **arg).collect();
        let args = &args[..];

        let vararg = if self.vararg {
            builder::TRUE
        } else {
            builder::FALSE
        };

        unsafe {
            LLVMFunctionType(ret, args.as_ptr() as *mut LLVMTypeRef, self.args.len() as libc::c_uint,
                             vararg)
        }
    }

    pub fn ret(&self) -> Type {
        self.ret
    }
}

pub trait NumExt {
    fn to_llvm(&self, cx: &Context) -> LLVMValueRef;
}

macro_rules! num_ext {
    ( $( $num: tt, ) * ) => {
    $(
        impl NumExt for $num {
            fn to_llvm( & self, cx: & Context) -> LLVMValueRef {
                Type::$num(cx).const_int(*self as i32)
            }
        }
    )*
    }
}

num_ext!(i8, i16, i32, i64,);
