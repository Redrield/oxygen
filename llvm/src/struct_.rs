use type_::*;
use context::Context;
use builder::Builder;

use llvm_sys::core::*;
use llvm_sys::prelude::*;

use std::ffi::CString;

#[derive(Clone, Debug)]
pub struct StructType {
    pub name: String,
    pub rf: LLVMTypeRef,
    pub members: Vec<String>,
    pub member_types: Vec<Type>,
}

#[derive(Clone, Debug)]
pub struct Struct<'a> {
    pub ty: &'a StructType,
    pub rf: LLVMValueRef,
}

impl StructType {
    pub fn new(name: String, members: Vec<(String, Type)>, cx: &Context) -> StructType {
        let rf = unsafe {
            let name = CString::new(name.clone()).unwrap();
            let mut members = members.iter().map(|(_, ty)| **ty).collect::<Vec<LLVMTypeRef>>();

            let ty = LLVMStructCreateNamed(cx.llcx, name.as_ptr());
            LLVMStructSetBody(ty, members.as_mut_ptr(), members.len() as u32, false as i32);
            ty
        };

        let member_types = members.iter().map(|(_, ty)| *ty).collect::<Vec<Type>>();
        let members = members.iter().map(|(name, _)| name.to_string()).collect::<Vec<String>>();

        StructType {
            name,
            rf,
            members,
            member_types
        }
    }

    pub fn instantiate<'a>(&'a self, values: Vec<LLVMValueRef>, bx: &Builder, cx: &Context) -> Struct<'a> {
        let ptr = bx.alloca(Type::from_ref(self.rf));

        for (i, value) in values.iter().enumerate() {
            let ptr = bx.struct_gep(ptr, i as u64);

            unsafe {
                if Type::from_ref(LLVMTypeOf(ptr)) == Type::i8(cx).ptr_to().ptr_to() {
                    bx.store(bx.inbounds_gep(*value, &[0.to_llvm(cx), 0.to_llvm(cx)]), ptr);
                } else {
                    bx.store(*value, ptr);
                }
            }
        }

        Struct {
            ty: self,
            rf: bx.load(ptr),
        }
    }
}

impl<'a> Struct<'a> {
    pub fn get(&self, name: String, bx: &Builder) -> Option<LLVMValueRef> {
        let index = self.ty.members.iter().position(|x| *x == name)?;
        let value = bx.struct_gep(self.rf, index as u64);
        Some(bx.load(value))
    }
}
